# number of factor
import numpy as np
from random import randint
from random import seed


def gcd(a, b):
    while b != 0:
        t = b
        b = a % b
        a = t
    return a

#find a divisor, not all

def a_pow_b_mod_n(a,b,n):
    aux = b
    result = 1
    while(aux > 0):
        result = result * a
        aux = aux - 1
        aux = aux % n
    return result

def runSchor(N, a):

    b = gcd(a, N)
    if b != 1:
        return []
    y = a
    cnt = 1

    while y != 1:
        y = (y * a) % N
        cnt = cnt + 1

    if cnt % 2 == 1:
        return []
    if a_pow_b_mod_n(a, cnt / 2, N) == -1:
        return []

    return [gcd(a_pow_b_mod_n(a, cnt / 2, N) + 1, N), gcd(a_pow_b_mod_n(a, cnt / 2, N) - 1, N)]





#number to factorize
N = 7687 * 1597
v = []
#random number
seed(120)
a = randint(2, N - 1)

result = runSchor(N, a)
print(result)
